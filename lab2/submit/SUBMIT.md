
Lab 2: Flow and Congestion Control/Experiment Design
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 2 - Flow and Congestion Control
-------------- | --------------------------------
Name           | Jay Patel
Net ID         | N11589789
Report due     | Sunday, 22 February 11:59PM


Please answer the following questions:

1) Describe (as specifically as possible) the *goal* of your
experiment. What makes this a good goal? Do not just repeat
all of the indicators of a "good" goal described in the lecture;
explain *how* your selected goal embodies those indicators.

**Ans 1)** The goal of this experiment is to evaluate the intra-protocol fairness by 
evaluating throughput of bic,scalable and reno for two parallel 
flows, four parallel flows,eight parallel flows and ten parallel flows over 1Gbps 
link with 140ms RTT and transmission time 60s.

I think this goal is a good goal since it specifies what experiment is to be conducted, which metrice is to be studied
and which parameters have to be used. There is no ambiguity on what to perform.  


2) What is the TCP congestion control variant you have selected
as the main subject of your study? Describe briefly (1 paragraph)
what characterizes this TCP congestion control, and how it works.
Cite your sources.

**Ans 2)** BIC is the TCP congestion control variant selected,it aims at
improving the preformance of conventional TCP over LFN's by implementing 
more aggressive window increase mechanism called the binary search
increase over AIMD(additive increase multiplicative incresae)mechanism.

This (Binary search) mechanism maintains two bounds upper (Wmax) and 
lower bound (Wmin)at two speeds at which the packet loss starts and at which there is no
packet loss respectively.Best value of the window is set between these
two values repeatedly depending on the feedback.Based on the feedback 
the midpoint is taken as the new Wmax if there is a packet loss, and 
as the new Wmin if not.Rate of increase in the window is large when the 
difference between Wmin and Wmax is large and rate decreases as the 
difference decreases in order to prevent packet loss.

**resource:** 

[http://www.hindawi.com/journals/jcnc/2012/806272/](http://www.hindawi.com/journals/jcnc/2012/806272/)

[http://www.researchgate.net/profile/Dino_Lopez_Pacheco/publication/250451369_Performance_comparison_of_TCP_HSTCP_and_XCP_in_high-speed_highly_variable-bandwidth_environments/links/54a57cf00cf267bdb908241a.pdf?ev=pub_ext_doc_dl&origin=publication_detail&inViewer=true](http://www.researchgate.net/profile/Dino_Lopez_Pacheco/publication/250451369_Performance_comparison_of_TCP_HSTCP_and_XCP_in_high-speed_highly_variable-bandwidth_environments/links/54a57cf00cf267bdb908241a.pdf?ev=pub_ext_doc_dl&origin=publication_detail&inViewer=true)

[http://ieeexplore.ieee.org/ielx5/6260028/6269390/06269516.pdf?tp=&arnumber=6269516&isnumber=6269390](http://ieeexplore.ieee.org/ielx5/6260028/6269390/06269516.pdf?tp=&arnumber=6269516&isnumber=6269390)

[http://toolkit.globus.org/alliance/publications/papers/Survey.pdf](http://toolkit.globus.org/alliance/publications/papers/Survey.pdf)


3) What is the other LFN TCP you have selected to use in your
experiment? What is the non-LFN TCP you have selected to use
in your experiment? Be brief (1 paragraph each), and cite your
sources. Why did you choose these specifically?

**Ans 3)** Other TCP varaints selected for the experiment are Reno and Scalable.
Reno is the one selected for non-LFN network.
Reno uses AIMD mechanism which gives less throughput and the increase in the throughput
is not significant as the number of flows increase as compared to Binary search 
mechanism where the throughput increases significantly as the number of flows increase.
Choosing these two protocols can thereby give a better comparision.

**resource:** 

[http://www.hindawi.com/journals/jcnc/2012/806272/](http://www.hindawi.com/journals/jcnc/2012/806272/)

[http://www.researchgate.net/profile/Dino_Lopez_Pacheco/publication/250451369_Performance_comparison_of_TCP_HSTCP_and_XCP_in_high-speed_highly_variable-bandwidth_environments/links/54a57cf00cf267bdb908241a.pdf?ev=pub_ext_doc_dl&origin=publication_detail&inViewer=true](http://www.researchgate.net/profile/Dino_Lopez_Pacheco/publication/250451369_Performance_comparison_of_TCP_HSTCP_and_XCP_in_high-speed_highly_variable-bandwidth_environments/links/54a57cf00cf267bdb908241a.pdf?ev=pub_ext_doc_dl&origin=publication_detail&inViewer=true)

[http://ieeexplore.ieee.org/ielx5/6260028/6269390/06269516.pdf?tp=&arnumber=6269516&isnumber=6269390](http://ieeexplore.ieee.org/ielx5/6260028/6269390/06269516.pdf?tp=&arnumber=6269516&isnumber=6269390)

[http://toolkit.globus.org/alliance/publications/papers/Survey.pdf](http://toolkit.globus.org/alliance/publications/papers/Survey.pdf)


4) Describe the parameters you have chosen to vary in your
experiment. Why did you choose these? How does this selection help further your stated goal?

**Ans 4)** The parameters chosen to vary in the experiment are number of flows and TCP variants.
Delay and the Capacity of the link are kept constant and the evaluation is taken over 60s 
transmission time for a particular tcp varient.

Choosing these parameters gives us the idea of Throughput performance of TCP variants with the change in number of parallel flows.

Keeping the delay and the capacity constant for a given TCP variant the throughput is measured 
for different flows 1,2,4,8 etc. Measuring the throughput for different number of parallel flows
gives us the idea about intra-protocol fairness.

5) What metrics are you going to measure in your experiment?
Why did you choose these? How does this selection help further your stated goal?

**Ans 5)** Throughput is the metrice we are going to measure for different number of parallel flows over 1Gbps
link and 140ms RTT for TCP variants Reno, Bic and Scalable.Throughput is measured using iperf tool.
Choosing Throughput and measuring it for different parallel flows gives us the idea of intra-protocol fairness.It also gives
us the idea on how significantly the throughput increases as the number of parallel flows increase.

6) For each **experimental unit** in your experiment, describe:

**Ans 6)**

* The specific parameters with which this experimental unit ran

**ans)** Link Capacity, Link RTT, TCP variats and the number of parallel TCP flows are the parameters with which the experimental unit ran.

* The names of all the data files which give results from this experimental unit. Include all of these files in this `submit` folder.

**ans)** Data files have format "TCP variant_RTT_number of flows"  eg: bic_140_1 indicates variant as BIC, RTT 140ms and 1 flow over link.

rps just indicates different report style, nlfn indicates evaluation over a non-LFN network.

**All the data files have been included in folder "final" and "final_2" in "submit" folder.**

* The specific values of the metrics you have chosen to measure.

**ans)** specific values of matrice throughput for differnt set of parallel flows choosen to measure.

Protocol  |1 flow  |2 flows | 4 flows | 8 flows|10 flows|
----------|--------|--------|---------|--------|--------|
Reno      |7.12Mbps|14.1Mbps|28.4Mbps |55.2Mbps|70.9Mbps|
Bic       |10.0Mbps|20.2Mbps|40.4Mbps |80.9Mbps|101Mbps |
Scalble   |7.08Mbps|14.1Mbps|28.4Mbps |55.3Mbps|71Mbps  | 


7) Describe any evidence of *interactions* you can see in the results of your experiment.

**Ans 7)**Throughput is the matrice being measured for differnt values of parallel flows. Throughput increases as the number of parallel flows increase 
for a given set of fixed parameters.

8) Briefly describe the results of your experiment (1-2 paragraphs). You may include images by putting them on an online
image hosting service, then putting the image URL in this file
using the syntax

![](http://link/to/image.png)

**Ans 8)** The aggregate throughput of all the
evaluated protocols increases with the number of parallel flows
increasing. The increase in throughput is not significant in case of Reno and Scalable TCP as 
compared to TCP BIC.


9) Find a published paper that studies the same TCP congestion
control variant you have chosen (it may study others as well).
Identify the paper you have chosen with a full citation, and
briefly answer the following questions about it:

 * What research question does this study seek to answer?

**Ans)** The paper mainly evaluates the TCP-based improved protocols in the Linux
2.6.18 kernel by establishing point-to-point and mUltipoint-topoint
test beds. They compare the effect of RTT and packet loss
rate to the transmission performance of the TCP-based protocols.
They also evaluate the intra-protocol fairness, TCP-friendliness
and inter-RTT fairness of these protocols.

 * What kind of network environment was this study conducted in?

**Ans)** The performance of TCP-based protocols by was evaluated by
establishing the test bed. FLDnet test bed is
constituted by terminal servers and a NETEM simulation,
between which through gigabit Ethernet links connect. The
NETEM simulation can change the network link bandwidth,
delay, loss ratio and other network parameters. These TCPbased
improved protocols are implemented in Linux 2.6.18
kernel.
 
 * How representative is the above network environment of the network setting this TCP variant is designed for?

**Ans)** The network environment seems representative since the evaluation is being made for tcp based high speed transfer protocols and 
 the environment and link set up is a Long Fat Network. 
  
 * Does this study make some comparison to another TCP congestion
 control algorithm? If so, which, and does the author explain why these were selected?
 
 **Ans)** Yes, this study makes some comparison to another TCP congestion
 control algorithm like HSTCP, Vegas, HTCP, Cubic, Westwood etc. 
 No, the author does not explain why these were selected.
 
 * What parameters does the author of this study consider? Does the
 author explain why?

**Ans)** Link Capacity, Link RTT, number of parallel flows, TCP variants, loss packet ratio etc are the parameters the author considers for the study.
 No, the author doesnt explain as to why he selects these parameters.
 
 * What metrics does the author of this study consider? Does the author explain why?

**Ans)** Throughput and Intra protocol fairness ratio are the metrices author considers. 
Yes, Author explains as to why these metrices have been selected.

* Critique the experiment(s) in the paper. Does it make any of
 the common mistakes described in the lab lecture? Explain.

**Ans)** The author doesn't describe as to why he choose particular set of parameters in the experiment. 
 It must specify as to why these were choosen and how were they measured.
 Which linux tools were used for measurement of the parameters has not been specified.
 
