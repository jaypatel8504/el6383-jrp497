------------------------------------------------------------
Client connecting to 10.1.1.2, TCP port 5001
TCP window size: 23.5 KByte (default)
------------------------------------------------------------
[  4] local 10.1.1.1 port 58005 connected with 10.1.1.2 port 5001
[  5] local 10.1.1.1 port 58006 connected with 10.1.1.2 port 5001
[  3] local 10.1.1.1 port 58004 connected with 10.1.1.2 port 5001
[  6] local 10.1.1.1 port 58007 connected with 10.1.1.2 port 5001
[ ID] Interval       Transfer     Bandwidth
[  6]  0.0- 1.0 sec   384 KBytes  3.15 Mbits/sec
[  4]  0.0- 1.0 sec   384 KBytes  3.15 Mbits/sec
[  5]  0.0- 1.0 sec   384 KBytes  3.15 Mbits/sec
[  3]  0.0- 1.0 sec   384 KBytes  3.15 Mbits/sec
[SUM]  0.0- 1.0 sec  1.50 MBytes  12.6 Mbits/sec
[  6]  1.0- 2.0 sec   384 KBytes  3.15 Mbits/sec
[  4]  1.0- 2.0 sec   384 KBytes  3.15 Mbits/sec
[  5]  1.0- 2.0 sec   384 KBytes  3.15 Mbits/sec
[  3]  1.0- 2.0 sec   384 KBytes  3.15 Mbits/sec
[SUM]  1.0- 2.0 sec  1.50 MBytes  12.6 Mbits/sec
[  3]  2.0- 3.0 sec   384 KBytes  3.15 Mbits/sec
[  4]  2.0- 3.0 sec   512 KBytes  4.19 Mbits/sec
[  5]  2.0- 3.0 sec   512 KBytes  4.19 Mbits/sec
[  6]  2.0- 3.0 sec   512 KBytes  4.19 Mbits/sec
[SUM]  2.0- 3.0 sec  1.88 MBytes  15.7 Mbits/sec
[  4]  3.0- 4.0 sec   768 KBytes  6.29 Mbits/sec
[  5]  3.0- 4.0 sec   768 KBytes  6.29 Mbits/sec
[  3]  3.0- 4.0 sec   640 KBytes  5.24 Mbits/sec
[  6]  3.0- 4.0 sec   384 KBytes  3.15 Mbits/sec
[SUM]  3.0- 4.0 sec  2.50 MBytes  21.0 Mbits/sec
[  4]  4.0- 5.0 sec   768 KBytes  6.29 Mbits/sec
[  5]  4.0- 5.0 sec   768 KBytes  6.29 Mbits/sec
[  6]  4.0- 5.0 sec   512 KBytes  4.19 Mbits/sec
[  3]  4.0- 5.0 sec   640 KBytes  5.24 Mbits/sec
[SUM]  4.0- 5.0 sec  2.62 MBytes  22.0 Mbits/sec
[  3]  5.0- 6.0 sec   768 KBytes  6.29 Mbits/sec
[  6]  5.0- 6.0 sec   768 KBytes  6.29 Mbits/sec
[  4]  5.0- 6.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5]  5.0- 6.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM]  5.0- 6.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4]  6.0- 7.0 sec   896 KBytes  7.34 Mbits/sec
[  5]  6.0- 7.0 sec   896 KBytes  7.34 Mbits/sec
[  3]  6.0- 7.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6]  6.0- 7.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM]  6.0- 7.0 sec  3.75 MBytes  31.5 Mbits/sec
[  4]  7.0- 8.0 sec   768 KBytes  6.29 Mbits/sec
[  5]  7.0- 8.0 sec   768 KBytes  6.29 Mbits/sec
[  3]  7.0- 8.0 sec   896 KBytes  7.34 Mbits/sec
[  6]  7.0- 8.0 sec   768 KBytes  6.29 Mbits/sec
[SUM]  7.0- 8.0 sec  3.12 MBytes  26.2 Mbits/sec
[  3]  8.0- 9.0 sec   768 KBytes  6.29 Mbits/sec
[  4]  8.0- 9.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5]  8.0- 9.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6]  8.0- 9.0 sec   896 KBytes  7.34 Mbits/sec
[SUM]  8.0- 9.0 sec  3.62 MBytes  30.4 Mbits/sec
[  4]  9.0-10.0 sec   896 KBytes  7.34 Mbits/sec
[  5]  9.0-10.0 sec   896 KBytes  7.34 Mbits/sec
[  6]  9.0-10.0 sec   896 KBytes  7.34 Mbits/sec
[  3]  9.0-10.0 sec  1.12 MBytes  9.44 Mbits/sec
[SUM]  9.0-10.0 sec  3.75 MBytes  31.5 Mbits/sec
[  3] 10.0-11.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 10.0-11.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 10.0-11.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 10.0-11.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 10.0-11.0 sec  3.75 MBytes  31.5 Mbits/sec
[  3] 11.0-12.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 11.0-12.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 11.0-12.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 11.0-12.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 11.0-12.0 sec  3.50 MBytes  29.4 Mbits/sec
[  6] 12.0-13.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 12.0-13.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 12.0-13.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 12.0-13.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 12.0-13.0 sec  3.25 MBytes  27.3 Mbits/sec
[  4] 13.0-14.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 13.0-14.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 13.0-14.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 13.0-14.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 13.0-14.0 sec  3.50 MBytes  29.4 Mbits/sec
[  3] 14.0-15.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 14.0-15.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 14.0-15.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 14.0-15.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 14.0-15.0 sec  3.75 MBytes  31.5 Mbits/sec
[  6] 15.0-16.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 15.0-16.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 15.0-16.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 15.0-16.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 15.0-16.0 sec  3.25 MBytes  27.3 Mbits/sec
[  4] 16.0-17.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 16.0-17.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 16.0-17.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 16.0-17.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 16.0-17.0 sec  3.50 MBytes  29.4 Mbits/sec
[  3] 17.0-18.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 17.0-18.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 17.0-18.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 17.0-18.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 17.0-18.0 sec  3.75 MBytes  31.5 Mbits/sec
[  4] 18.0-19.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 18.0-19.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 18.0-19.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 18.0-19.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 18.0-19.0 sec  3.75 MBytes  31.5 Mbits/sec
[  3] 19.0-20.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 19.0-20.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 19.0-20.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 19.0-20.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 19.0-20.0 sec  3.75 MBytes  31.5 Mbits/sec
[  4] 20.0-21.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 20.0-21.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 20.0-21.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 20.0-21.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 20.0-21.0 sec  3.25 MBytes  27.3 Mbits/sec
[  4] 21.0-22.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 21.0-22.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 21.0-22.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 21.0-22.0 sec   768 KBytes  6.29 Mbits/sec
[SUM] 21.0-22.0 sec  3.38 MBytes  28.3 Mbits/sec
[  3] 22.0-23.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 22.0-23.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 22.0-23.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 22.0-23.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 22.0-23.0 sec  3.75 MBytes  31.5 Mbits/sec
[  6] 23.0-24.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 23.0-24.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 23.0-24.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 23.0-24.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 23.0-24.0 sec  3.25 MBytes  27.3 Mbits/sec
[  3] 24.0-25.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 24.0-25.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 24.0-25.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 24.0-25.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 24.0-25.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 25.0-26.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 25.0-26.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 25.0-26.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 25.0-26.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 25.0-26.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 26.0-27.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 26.0-27.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 26.0-27.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 26.0-27.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 26.0-27.0 sec  3.25 MBytes  27.3 Mbits/sec
[  4] 27.0-28.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 27.0-28.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 27.0-28.0 sec  1.00 MBytes  8.39 Mbits/sec
[  3] 27.0-28.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 27.0-28.0 sec  4.00 MBytes  33.6 Mbits/sec
[  4] 28.0-29.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 28.0-29.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 28.0-29.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 28.0-29.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 28.0-29.0 sec  3.50 MBytes  29.4 Mbits/sec
[  3] 29.0-30.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 29.0-30.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 29.0-30.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 29.0-30.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 29.0-30.0 sec  3.75 MBytes  31.5 Mbits/sec
[  4] 30.0-31.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 30.0-31.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 30.0-31.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 30.0-31.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 30.0-31.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 31.0-32.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 31.0-32.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 31.0-32.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 31.0-32.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 31.0-32.0 sec  3.25 MBytes  27.3 Mbits/sec
[  3] 32.0-33.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 32.0-33.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 32.0-33.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 32.0-33.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 32.0-33.0 sec  3.62 MBytes  30.4 Mbits/sec
[  4] 33.0-34.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 33.0-34.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 33.0-34.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 33.0-34.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 33.0-34.0 sec  3.62 MBytes  30.4 Mbits/sec
[  4] 34.0-35.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 34.0-35.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 34.0-35.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 34.0-35.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 34.0-35.0 sec  3.25 MBytes  27.3 Mbits/sec
[  6] 35.0-36.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 35.0-36.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 35.0-36.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 35.0-36.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 35.0-36.0 sec  3.50 MBytes  29.4 Mbits/sec
[  3] 36.0-37.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 36.0-37.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 36.0-37.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 36.0-37.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 36.0-37.0 sec  3.62 MBytes  30.4 Mbits/sec
[  4] 37.0-38.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 37.0-38.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 37.0-38.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 37.0-38.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 37.0-38.0 sec  3.62 MBytes  30.4 Mbits/sec
[  3] 38.0-39.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 38.0-39.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 38.0-39.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 38.0-39.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 38.0-39.0 sec  3.88 MBytes  32.5 Mbits/sec
[  4] 39.0-40.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 39.0-40.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 39.0-40.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 39.0-40.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 39.0-40.0 sec  3.38 MBytes  28.3 Mbits/sec
[  4] 40.0-41.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 40.0-41.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 40.0-41.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 40.0-41.0 sec   768 KBytes  6.29 Mbits/sec
[SUM] 40.0-41.0 sec  3.25 MBytes  27.3 Mbits/sec
[  3] 41.0-42.0 sec   896 KBytes  7.34 Mbits/sec
[  4] 41.0-42.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 41.0-42.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 41.0-42.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 41.0-42.0 sec  3.88 MBytes  32.5 Mbits/sec
[  4] 42.0-43.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 42.0-43.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 42.0-43.0 sec   896 KBytes  7.34 Mbits/sec
[  3] 42.0-43.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 42.0-43.0 sec  3.38 MBytes  28.3 Mbits/sec
[  4] 43.0-44.0 sec   896 KBytes  7.34 Mbits/sec
[  5] 43.0-44.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 43.0-44.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 43.0-44.0 sec   768 KBytes  6.29 Mbits/sec
[SUM] 43.0-44.0 sec  3.25 MBytes  27.3 Mbits/sec
[  3] 44.0-45.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 44.0-45.0 sec  1.00 MBytes  8.39 Mbits/sec
[  4] 44.0-45.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 44.0-45.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 44.0-45.0 sec  3.88 MBytes  32.5 Mbits/sec
[  4] 45.0-46.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 45.0-46.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 45.0-46.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 45.0-46.0 sec   896 KBytes  7.34 Mbits/sec
[SUM] 45.0-46.0 sec  3.38 MBytes  28.3 Mbits/sec
[  4] 46.0-47.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 46.0-47.0 sec  1.00 MBytes  8.39 Mbits/sec
[  3] 46.0-47.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 46.0-47.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 46.0-47.0 sec  4.00 MBytes  33.6 Mbits/sec
[  6] 47.0-48.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 47.0-48.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 47.0-48.0 sec  1.00 MBytes  8.39 Mbits/sec
[  3] 47.0-48.0 sec   768 KBytes  6.29 Mbits/sec
[SUM] 47.0-48.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 48.0-49.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 48.0-49.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 48.0-49.0 sec   896 KBytes  7.34 Mbits/sec
[  6] 48.0-49.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 48.0-49.0 sec  3.38 MBytes  28.3 Mbits/sec
[  3] 49.0-50.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 49.0-50.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 49.0-50.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 49.0-50.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 49.0-50.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 50.0-51.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 50.0-51.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 50.0-51.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 50.0-51.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 50.0-51.0 sec  3.50 MBytes  29.4 Mbits/sec
[  3] 51.0-52.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 51.0-52.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 51.0-52.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 51.0-52.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 51.0-52.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 52.0-53.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 52.0-53.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 52.0-53.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 52.0-53.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 52.0-53.0 sec  3.50 MBytes  29.4 Mbits/sec
[  3] 53.0-54.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 53.0-54.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 53.0-54.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 53.0-54.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 53.0-54.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 54.0-55.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 54.0-55.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 54.0-55.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 54.0-55.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 54.0-55.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 55.0-56.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 55.0-56.0 sec  1.00 MBytes  8.39 Mbits/sec
[  3] 55.0-56.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 55.0-56.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 55.0-56.0 sec  4.00 MBytes  33.6 Mbits/sec
[  3] 56.0-57.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 56.0-57.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 56.0-57.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 56.0-57.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 56.0-57.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 57.0-58.0 sec   768 KBytes  6.29 Mbits/sec
[  5] 57.0-58.0 sec   768 KBytes  6.29 Mbits/sec
[  3] 57.0-58.0 sec  1.00 MBytes  8.39 Mbits/sec
[  6] 57.0-58.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 57.0-58.0 sec  3.50 MBytes  29.4 Mbits/sec
[  3] 58.0-59.0 sec   768 KBytes  6.29 Mbits/sec
[  6] 58.0-59.0 sec   768 KBytes  6.29 Mbits/sec
[  4] 58.0-59.0 sec  1.00 MBytes  8.39 Mbits/sec
[  5] 58.0-59.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 58.0-59.0 sec  3.50 MBytes  29.4 Mbits/sec
[  4] 59.0-60.0 sec   768 KBytes  6.29 Mbits/sec
[  4]  0.0-60.1 sec  51.6 MBytes  7.21 Mbits/sec
[  5] 59.0-60.0 sec   768 KBytes  6.29 Mbits/sec
[  5]  0.0-60.1 sec  51.6 MBytes  7.21 Mbits/sec
[  3] 59.0-60.0 sec  1.00 MBytes  8.39 Mbits/sec
[  3]  0.0-60.2 sec  51.1 MBytes  7.12 Mbits/sec
[  6] 59.0-60.0 sec  1.00 MBytes  8.39 Mbits/sec
[SUM] 59.0-60.0 sec  3.50 MBytes  29.4 Mbits/sec
[  6]  0.0-60.2 sec  50.9 MBytes  7.09 Mbits/sec
[SUM]  0.0-60.2 sec   205 MBytes  28.6 Mbits/sec
