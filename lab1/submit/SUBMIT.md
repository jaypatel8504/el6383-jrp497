
## Submission

1) The following shows the `ifconfig` command and its output for the client and server in **my** experiment.
###Server
```
jrp497@server:~$ ifconfig
eth0      Link encap:Ethernet  HWaddr 02:cb:42:78:39:d5
          inet addr:172.17.2.2  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::cb:42ff:fe78:39d5/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:4177 errors:0 dropped:1 overruns:0 frame:0
          TX packets:453 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:222634 (222.6 KB)  TX bytes:54611 (54.6 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:b8:8a:ad:2f:e5
          inet addr:10.1.1.2  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::b8:8aff:fead:2fe5/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:6 errors:0 dropped:0 overruns:0 frame:0
          TX packets:9 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:384 (384.0 B)  TX bytes:978 (978.0 B)
          Interrupt:26

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:4 errors:0 dropped:0 overruns:0 frame:0
          TX packets:4 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:260 (260.0 B)  TX bytes:260 (260.0 B)
```
###Client
```
jrp497@client:~$ ifconfig
eth0      Link encap:Ethernet  HWaddr 02:36:9a:9c:05:e2
          inet addr:172.17.2.1  Bcast:172.31.255.255  Mask:255.240.0.0
          inet6 addr: fe80::36:9aff:fe9c:5e2/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:5159 errors:0 dropped:2 overruns:0 frame:0
          TX packets:427 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:257065 (257.0 KB)  TX bytes:48548 (48.5 KB)
          Interrupt:25

eth1      Link encap:Ethernet  HWaddr 02:11:7d:5d:6a:b4
          inet addr:10.1.1.1  Bcast:10.1.1.255  Mask:255.255.255.0
          inet6 addr: fe80::11:7dff:fe5d:6ab4/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:21 errors:0 dropped:0 overruns:0 frame:0
          TX packets:10 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:1620 (1.6 KB)  TX bytes:1048 (1.0 KB)
          Interrupt:26

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:16436  Metric:1
          RX packets:4 errors:0 dropped:0 overruns:0 frame:0
          TX packets:4 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:260 (260.0 B)  TX bytes:260 (260.0 B)
```


2)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Node    | control interface name | control IP | data interface name | data IP
------- | ---------------------- | ---------- | ------------------- | --------
client  |     **eth0**         |**172.17.2.1**|     **eth1**        |**10.1.1.1**
server  |     **eth0**         |**172.17.2.2**|     **eth1**        |**10.1.1.2**

3)  Fill out the missing cells in the following table, using the results
of your individual experiment:

Link     |  Bandwidth (Mbits/sec)
-------- | ----------------------
control  |  **4485.12 Mbits/sec  (4.38 Gbits/sec)**
data     |  **100 Mbits/sec**


4) What happens if you bring down the data interface of your node using the command

    sudo ifconfig DATAIFNAME down

 where `DATAIFNAME` is the name of the data interface (i.e. `eth0` or `eth1`)? What happens if you bring down the control interface of your node? Why?

###**Ans.4)**
If we bring down the data interface i.e eth1, it is disabled and we cannot send data between the client and the server.Destination unreachable message is prompted on pinging.

However bringing down the control interface i.e eth0, we lose the connectivity. Host loses control (connectivity).ssh session hangs. We cannot login to our nodes, we need to delete our resources and start again.