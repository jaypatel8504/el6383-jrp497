
Lab 3: Routing and resiliency
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment | 3 - Routing and resiliency
-------------- | --------------------------------
Name           | **Jay Patel** 
Net ID         | **jrp497**
Report due     | Sunday, 29 March 11:59PM


Please answer the following questions:


## Dijkstra's algorithm experiment

**Q1.** Were you able to successfully produce experiment results? If so, show a screenshot of your experiment results (topology + completed table).

**Ans 1.** Yes, I was successfully able to produce the experiment results.
###Topology
![Topology](http://i.imgur.com/c0COs24.png?1)
###Table
![Table](http://i.imgur.com/hPrIQkZ.png?1)  

**Q2.** How long did it take you to run this experiment, from start (create an account) to finish (getting a screenshot of results)?

**Ans 2.** It didn't take much of my time. Approx 20 minutes, from creating the account, going through the information and video and finally getting the screenshots of the results.

**Q3.** Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

**Ans 3.** No, I did not need to make any changes or do additional steps beyond the documentation.

**Q4.** In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

**Ans 4.** The results of this experiment can be easily reproduced by an independent researcher with at most 15 min of user effort, requiring only standard, freely available tools (web-based interface on hyperion.poly.edu). It falls on the 5th degree of reproducibility.

**Q5.** Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if there was no web-based interface on hyperion.poly.edu?

**Ans 5.** If there was no web-based interface on hyperion.poly.edu and only the material an documentation were provided it would take me a day or so to analyse, set up, find solutins to problems that may arise or find alternate paths and run the experiment. It would have fallen in the 3rd Degree of Reproducibility - "requiring considerable effort to reproduce"
 
## OSPF experiment

**Q1.** Were you able to successfully produce experiment results? If so, show your experiment results. You should have:

**Ans 1.** Yes, I was succesfully able to reproduce the experiment results.

###Traceroute from client to server with all links active
![Traceroute from client to server with all links active](http://i.imgur.com/FfoIWs2.png)
###OSPF table on router-1 with all links active
![OSPF table on router-1 with all links active](http://i.imgur.com/2fOria3.png)
###Traceroute from client to server with the router-2 link down
![Traceroute from client to server with the router-2 link down](http://i.imgur.com/wCiYAEa.png)
###OSPF table on router-1 with the router-2 link down
![OSPF table on router-1 with the router-2 link down](http://i.imgur.com/giKOc7Q.png)
![Imgur](http://i.imgur.com/RLWRB3G.png)

**Q2.** How long did it take you to run this experiment, from start (reserve resources on GENI) to finish (getting the output to put in the previous )?

**Ans 2.** It took me almost 4 hours of effort from start (reserve resources on GENI) to finish (getting snapshots and editing the final output on bitbucket)

**Q3.** Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

**Ans 3.** No, I did not need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment. All information and resources were available in order to reproduce the experiment.

**Q4.** In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

**Ans 4.**  The results can be reproduced by an independent researcher, requiring considerable effort. I would therefore characterize this experiment on the 3rd degree of reproducibility.

**Q5.** Given the materials and documentation provided by the experiment designers, how long do you think it would take you to set up and run the experiment if the experiment artifacts (disk image, RSpec, etc.) were not provided for you?

**Ans 5.** Given the materials and documentation provided by the experiment designers, it would take me 2 days to set up and run the experiment if the experiment artifacts (disk image, RSpec, codes etc.) were not provided for me.
