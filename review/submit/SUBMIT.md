
Project Review
=====================================================

Please fill in your name and net ID in the table below.

Lab Assignment       | Project Review
-------------------- | --------------------------------
Name                 | **Jay R Patel**
Net ID               | **jrp497**
Assigned project (#) | **17**
Review due           | Monday, 4 May 11:59PM


Please write detailed comments answering the questions below.
Do not just answer "yes" or "no" - comment on *how well* the authors
address each of these aspects of experimentation, and offer 
suggestions for improvement.

## Overview

**1)** Briefly summarize the experiment in this project.

**Ans1.** The experiment conducted involves study of the packet processing time of two routers XORP and OVS against Linux Kernel router.
A simple topology has been designed where a server is connected to client through a router. Iperf tool has been used to generate traffic
between server and client. Tcpdump tool captures the traffic at the incoming and outgoing of the router. Time difference between the ingress 
and egress of the packet is calculated to know a pacrticular router's packet processing time. The output of tcpdump generates a .pcap file,
which is then converted to .csv file using tool Tshark. The data files so obtained have been processed and plotted using Rstudio.




**2)** Does the project generally follow the guidelines and parameters we have 
learned in class? 

**Ans2.** Not all the guidlines, experiment is consistent with the goal and does not deviate from what it needs to achieve except for the plots
(Analysis plan says "we observe how the delay varies depending upon the traffic" the plot however represents processing time for certain 
percentage of packets like "processing time for almost 97% packets is between 0.00003s to 0.00004s" which has no correlation with traffic) 
The level of details provided is inappropriate (like the Rspec file could not be accessed link opens up to filedropper). 
Not a clear picture as to why UDP is prefferd over TCP.
Packet size strictly less than 1470 has been specified, why taking packets with size more than 1470 would create problem in the experimental 
results has not been adressed (likely reason may be : inaccurate results due to Fragmentation). 
Level of deatils provided in the plots is nominal (given only the plot not much can be predicted as to what they represent.)



## Experiment design

**1)** What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

**Ans1.** The goal of the experiment is to compare the packet processing time of the following routers: 1.XORP 2.OVS with local flow 3.Linux kernel Router.
Goal seems to be specific since the packet processing time of the routers is studies. It also doesnt seem to be biased. It is likely to give
interesting and useful results. What is expected is the time taken by a router to process a packet, collectively what percentage of packets, in
a flow during analysis, were processed in what range of delay. It is expected to give some meaningful outcome.

**2)** Are the metric(s) and parameters chosen appropriate for this 
experiment goal? Does the experiment design clearly support the experiment goal?

**Ans2.** Matrices choosen were iperf, tcpdump and tshark. Parameter choosen was tye of the router. Parameter and Matrices choosen were appropriate.
Iperf tool was used for  generating traffic from server to client, tcpdump was used on router to capture the traffic and tshark tool was used to 
convert the .pcap file to .csv file. However tcpdump tool was not much needed, since Tshark is a network protocol analyser and works much like 
tcpdump. It can capture traffic in .pcap format same as tcpdump. Using only Tshark would suffice. More info on Tshark options could be found here
[https://www.wireshark.org/docs/man-pages/tshark.html](https://www.wireshark.org/docs/man-pages/tshark.html).


**3)** Is the experiment well designed to obtain maximum information with the 
minimum number of trials?

**Ans3.** The experiment design is appropriate as far as getting maximun information with minimum trials is concerned, but the depth of information 
provided is less making it a bit difficult to reproduce. It would be more helpful,had it been more optimised.


**4)** Are the metrics selected for study the *right* metrics? Are they clear, 
unambiguous, and likely to lead to correct conclusions? Are there other 
metrics that might have been better suited for this experiment?

**Ans4.** The metrices selected for the study are appropriate and unambiguous one's and lead to correct conclusion. However tcpdump tool was not much needed,
since Tshark is a network protocol analyser and works much like tcpdump.
It can capture traffic in .pcap format same as tcpdump. Using only Tshark would suffice. More info on Tshark options could be found here
[https://www.wireshark.org/docs/man-pages/tshark.html](https://www.wireshark.org/docs/man-pages/tshark.html).


**5)** Are the parameters of the experiment meaningful? Are the ranges 
over which parameters vary meaningful and representative?

**Ans5.** Parameter of the expreiment is just the router variants and do not fall into the category of parameters with varying ranges.They work with
fixed configuration and seem appropriate.


**6)**Have the authors sufficiently addressed the possibility of interactions 
between parameters?

**Ans6.**No, the possiblity of interactions between the parameters has not been addressed and infact i think it is not needed since the parameters 
are independent and do not interact.


**7)** Are comparisons made reasonably? Is the baseline selected for comparison appropriate 
and realistic? 

**Ans7.** Yes, the comparisons have been made reasonably and the baseline selected for comparison  is appropriate 
and realistic.

## Communicating results


**1)** Do the authors report the quantitative results of their experiment?

**Ans1.** Yes, the quantitativ results have been presented by the authors. Output files have been genearted and plots have also been presented. 


**2)** Is there information given about the variation and/or distribution of 
experimental results?

**Ans2.** No, there is no information given about the variation and distribution of 
experimental results.


**3)** Do the authors practice *data integrity* - telling the truth about their data, 
avoiding ratio games and other practices to artificially make their results seem better?

**Ans3.** Yes, the data seems authentic and has been appropriately processed. Data has been collected using tcpdump, put into appropriate format using
tshark so that it becomes easy to process in Rstudio.


**4)** Is the data presented in a clear and effective way? If the data is presented in 
graphical form, is the type of graph selected appropriate for the "story" that 
the data is telling? 

**Ans4.** The data files obtained are appropriate. Plots however seems obscure.
(Analysis plan says "we observe how the delay varies depending upon the traffic" which apparently indicates depending on the traffic processing 
delay should vary: like under heavy traffic processing delay is more or so, which is not in coordination with the goal.
The plot  however represents processing time for certain percentage of packets like "processing time for almost 97% packets is between 0.00003s to 0.00004s" which has no correlation with traffic)

Level of deatils provided in the plots is nominal (given only the plot not much can be predicted as to what they represent and deatils seem wispy)


**5)** Are the conclusions drawn by the authors sufficiently supported by the 
experiment results?

**Ans5.** The conclusions drawn out of the experiment results by the authors are appropriate for example: the plot for the linux router indicates  
that 98 out of 100 packets were processed in 0.00002 sec. 

## Reproducible research



**1)** Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results, 
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

**Ans1.** Yes, the authors have indicated all the three ways for reprodicing the experimental results. 
The instructions are clear and easy to understand. Had they given what output would look like at certain points, it would have been more helpful. 

**2)** Were you able to successfully produce experiment results? 

**Ans2.** I was not succesfully able to reproduce the experimental results, partially I could achieve certain checkpoints. 
I was facing certain errors and tried ro resolve them. Also, the Rspec link mentioned by the authors doesn't open up to give the Rspec configuration.
I couldn't log in to the existing setup. Analysis of raw data was easy however.


**3)** How long did it take you to run this experiment, from start to finish?

**Ans3.** It took me almost 15 hours in a go to analyse the experiment, working with the resources, reproducing results and forming up a review.


**4)** Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

**Ans4.** I could not access the Rspec through the link authors mentioned, so I tried to make a similar configuration as mentioned and run the same
set of parameters and matrices. I could generate the output files but not so appropriately. I have my existing steup on geni portal.  


**5)** In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

**Ans5.** I would characterize this experiment over 2rd degree of reproducibility- The results can be reproduced by an 
independent researcher, requiring extreme effort.


## Other comments to authors


Please write any other comments that you think might help the authors
of this project improve their experiment.


**Working more on presenting the report, providing more details, links and references would help reproduce results more easily and effectively.**












